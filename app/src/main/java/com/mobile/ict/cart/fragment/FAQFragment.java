package com.mobile.ict.cart.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mobile.ict.cart.LokacartApplication;
import com.mobile.ict.cart.R;

/**
 * Created by vish on 21/3/16.
 */
public class FAQFragment extends Fragment {

    View FAQFragmentView;

    private Tracker mTracker;
    private static final String TAG = "FAQFragment";
    String name = "FAQ";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Obtain the shared Tracker instance.
        LokacartApplication application = (LokacartApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Fragment~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        FAQFragmentView = inflater.inflate(R.layout.fragment_faq, container, false);
        getActivity().setTitle(R.string.title_fragment_faq);

        setHasOptionsMenu(true);
        return FAQFragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}
