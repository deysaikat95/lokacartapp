package com.mobile.ict.cart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.mobile.ict.cart.container.Orders;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.interfaces.EditDeletePlacedOrderInterface;


import java.util.ArrayList;
import java.util.List;

public class PlacedOrderAdapter extends ExpandableRecyclerAdapter<OrderViewHolder,OrderItemViewHolder> {

    private LayoutInflater mInflator;
    Context context;
    Orders orders;
    int pos;
    String type;
    EditDeletePlacedOrderInterface editDeletePlacedOrderInterface;


    public PlacedOrderAdapter(Context context, @NonNull List<? extends ParentListItem> parentItemList,EditDeletePlacedOrderInterface editDeletePlacedOrderInterface,String type) {
        super(parentItemList);
        mInflator = LayoutInflater.from(context);
        this.context=context;
        this.type= type;
        this.editDeletePlacedOrderInterface = editDeletePlacedOrderInterface;
    }

    @Override
    public OrderViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View orderView = mInflator.inflate(R.layout.order_card_view, parentViewGroup, false);
        return new OrderViewHolder(orderView);
    }

    @Override
    public OrderItemViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View orderItemView = mInflator.inflate(R.layout.order_list, childViewGroup, false);
       return new OrderItemViewHolder(context,orderItemView, editDeletePlacedOrderInterface,type);
    }

    @Override
    public void onBindParentViewHolder(OrderViewHolder recipeViewHolder, int position, ParentListItem parentListItem) {
         orders = (Orders) parentListItem;
         pos= position;
         recipeViewHolder.bind(orders,type);

           }

    @Override
    public void onBindChildViewHolder(OrderItemViewHolder ingredientViewHolder, int position, Object childListItem) {
        ingredientViewHolder.bind((ArrayList<String[]>)childListItem, orders.getOrderDetails().getOrder_id(),pos,type);

    }
}
