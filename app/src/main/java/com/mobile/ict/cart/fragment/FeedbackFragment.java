package com.mobile.ict.cart.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mobile.ict.cart.LokacartApplication;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Organisations;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.JSONDataHelper;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vish on 21/3/16.
 */


/**
 * Created by Siddharthsingh on 06-05-2016.
 */
public class FeedbackFragment extends Fragment {


    final static String Lokacart_Tag = "Lokacart Team";
    int position =0;
    ArrayAdapter<String> adapter;
    List<String> list;
    EditText feedbackEditText;
    TextView charRemainingTV;
    private Tracker mTracker;
    private static final String TAG = "FeedbackFragment";
    String name = "Feedback";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the shared Tracker instance.
        LokacartApplication application = (LokacartApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Fragment~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        View view = inflater.inflate(R.layout.fragment_feedback, container, false);

        setHasOptionsMenu(true);

        //set title in actionbar
        getActivity().setTitle("Feedback");

        charRemainingTV = (TextView)view.findViewById(R.id.char_remaining_TV);
        Button submit = (Button)view.findViewById(R.id.submit_button);
        Spinner spinner = (Spinner) view.findViewById(R.id.planets_spinner);
        feedbackEditText = (EditText)view.findViewById(R.id.feedback_edittext);

        list  = new ArrayList<String>();
        list.add(getActivity().getString(R.string.spinner_feedback_loading_organisations));


        if(Master.isNetworkAvailable(getActivity())){

            try
            {
                JSONObject obj = new JSONObject();
                obj.put(Master.MOBILENUMBER,"91"+ MemberDetails.getMobileNumber());
                obj.put(Master.PASSWORD, MemberDetails.getPassword());
                new GetOrganisationsTask().execute(obj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }else{

            Toast.makeText(getActivity(),getActivity().getString(R.string.toast_Please_check_internet_connection), Toast.LENGTH_LONG).show();
            submit.setEnabled(false);

        }


        feedbackEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus){
                    try {
                        getActivity().getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        feedbackEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != -1)
                    charRemainingTV.setText((500 - s.length()) +" "+ getActivity().getString(R.string.pd_submitting_feedback_char_remaining));
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Message = feedbackEditText.getText().toString();


                //check if the feedbcak edit text has any text
                if(Message.trim().equals("")
                        ||Message.trim().isEmpty()){

                    Toast.makeText(getActivity(),getString(R.string.toast_feedback_enter_feedback_before_submitting),Toast.LENGTH_LONG).show();

                }else if(Message.equals(getString(R.string.spinner_feedback_loading_organisations))||Message.equals(getString(R.string.spinner_feedback_error_loading_organisations))){

                    Toast.makeText(getActivity(),getString(R.string.toast_feedback_error_loading_org),Toast.LENGTH_LONG).show();


                }else if(Message.equals(getString(R.string.spinner_feedback_select_organisation))){
                    Toast.makeText(getActivity(),getString(R.string.toast_feedback_select_organisation),Toast.LENGTH_LONG).show();

                }else if(position == 0){
                    Toast.makeText(getActivity(),getString(R.string.toast_feedback_select_organisation),Toast.LENGTH_LONG).show();
                }else{

                    String Orgabbr;
                    /*as Lokacart is not an organisation, its value will not be in Organisation.OrganisationList
                    so setting organisation manually */
                    if(list.get(position).equals(Lokacart_Tag)){
                        Orgabbr = "lcart";

                    }else {
                        Orgabbr = Organisations.organisationList.get(position-1).getOrgabbr();
                    }

                    try
                    {
                        JSONObject obj = new JSONObject();
                        obj.put(Master.MOBILENUMBER,"91"+ MemberDetails.getMobileNumber());
                        obj.put(Master.DEFAULT_ORG_ABBR, Orgabbr);
                        obj.put(Master.feedbackText, Message);

                        new SendFeedbackTask().execute(obj);


                    }
                    catch (JSONException e)
                    {

                        e.printStackTrace();
                    }


                }


            }
        });

        @DrawableRes int spinnerbg = R.drawable.downw_arrow;
        spinner.setBackgroundResource(spinnerbg);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                position = pos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        @DrawableRes int editTextShape = R.drawable.edittextshape;
        //setBackground was added in API level 16 so use setBackgroundDrawable for versions before that
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {

            spinner.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(),editTextShape,null));
            feedbackEditText.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(),editTextShape,null));

        } else {
            spinner.setBackground(ResourcesCompat.getDrawable(getResources(),editTextShape,null));
            feedbackEditText.setBackground(ResourcesCompat.getDrawable(getResources(),editTextShape,null));

        }




        // Create an ArrayAdapter using the string array and a textview that will be used in the spinner
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item , list);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        return view;

    }

    protected void AnalyticsEvent (String categoryId, String actionId) {

        // Get tracker.
        Tracker t = ((LokacartApplication) getActivity().getApplication()).getTracker(
                LokacartApplication.TrackerName.APP_TRACKER);
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory(categoryId)
                .setAction(actionId)
                //                                .setLabel(getString(labelId))
                .build());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    public class SendFeedbackTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_submitting_feedback), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            Map<String, String> articleParams = new HashMap<String, String>();

            //param keys and values have to be of String type
            articleParams.put("sendfeedbackdatasenttoserver", params[0].toString());
            FlurryAgent.logEvent("sendfeedbackdatasenttoserver",articleParams);
            GetJSON getJson = new GetJSON();
//            System.out.println(params[0]);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getSendCommentURL(), params[0], "POST", true,
                    MemberDetails.getEmail(), MemberDetails.getPassword());
//            System.out.println("Send Feedback Response "+Master.response);
//            Log.d("Send Feedback Response " , Master.response);
            //can't call Toast as it displays on UI from background thread
            // Toast.makeText(getContext(),Master.response,Toast.LENGTH_SHORT).show();
            return Master.response;

        }

        @Override
        protected void onPostExecute(String response) {
            Map<String, String> articleParams = new HashMap<String, String>();
            //param keys and values have to be of String type
            articleParams.put("sendfeedbackresponse", response);
            FlurryAgent.logEvent("sendfeedbackresponse",articleParams);
            Material.circularProgressDialog.dismiss();
//            System.out.println("------------" + response);
            if (response.equals("exception")) {
                Toast.makeText(getActivity() , getString(R.string.pd_submitting_feedback_error) , Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(getActivity() , getString(R.string.pd_submitting_feedback_success) , Toast.LENGTH_LONG).show();
                feedbackEditText.setText("");
                charRemainingTV.setText(500+getString(R.string.pd_submitting_feedback_char_remaining));
                SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, response);
                Master.hideSoftKeyboard(getActivity());
                //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new ProductFragment(), Master.PRODUCT_TAG).commit();
                DashboardActivity.updateDrawerPosition(0);
            }
        }
    }

    public class GetOrganisationsTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_fetching_data_from_server), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            GetJSON getJson = new GetJSON();
//            System.out.println(params[0]);
            Master.response = getJson.getJSONFromUrl(Master.getLoginURL(), params[0], "POST", false, null, null);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response) {
            Material.circularProgressDialog.dismiss();
//            System.out.println("------------" + response);
            if (response.equals("exception")) {
                //clear all previous values in the list and show the error
                list.clear();
                list.add(getString(R.string.spinner_feedback_error_loading_organisations));
                adapter.notifyDataSetChanged();
            }
            else
            {

                SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, response);
                Organisations.organisationList = new ArrayList<Organisations>();
                Organisations.organisationList = JSONDataHelper.getOrganisationListFromJson(getActivity(), SharedPreferenceConnector
                        .readString(getActivity().getApplicationContext(), Master.LOGIN_JSON, Master.DEFAULT_LOGIN_JSON));
                list.clear();

                if(Organisations.organisationList.size()>0){
                    list.add(getString(R.string.spinner_feedback_select_organisation));
                    for (int i = 0; i < Organisations.organisationList.size(); ++i) {

                        list.add(Organisations.organisationList.get(i).getName());

                    }
                    list.add(Lokacart_Tag);
                    adapter.notifyDataSetChanged();
                }else{

                    list.add(getString(R.string.spinner_feedback_error_loading_organisations));
                    adapter.notifyDataSetChanged();

                }



            }
        }
    }
}
