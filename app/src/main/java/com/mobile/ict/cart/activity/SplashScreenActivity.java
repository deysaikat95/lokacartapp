package com.mobile.ict.cart.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.flurry.android.FlurryAgentListener;
import com.mobile.ict.cart.TourCheck.DemoLists;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.util.Extra;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.SharedPreferenceConnector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by Siddharthsingh on 12-05-2016.
 */


public class SplashScreenActivity extends Activity {


    int debug = 0;
    static int from;
    static String goTo;

    DBHelper dbHelper;

    static JSONObject acceptReferralResponse;

    static String referOrgName;
    //check if a email string is actually an email
    public static boolean isValidEmail(@NonNull CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.logEvent("on start of splash screen");
        MemberDetails.setMemberDetails();
        dbHelper = new DBHelper(this);

        //check if app opened for the first time
        if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.STEPPER, Master.DEFAULT_STEPPER)) {
            //not setting the above shared preference to false because we have to check it is show stepper

            //SharedPreferenceConnector.writeBoolean(getApplicationContext(), Master.STEPPER, false);
            //opened for the first time

            //store update required value
            SharedPreferenceConnector.writeInteger(getApplicationContext() , Master.UpdateRequiredPref, -1);





        }

        //check if force update is set\ in shared pref
        if(Master.isNetworkAvailable(this)){

            Init();

        }else{

            if(SharedPreferenceConnector.readInteger(getApplicationContext(),Master.UpdateRequiredPref , -1)== 2){
                //show force update dialog
                forceUpdateDialog();
            }else if(SharedPreferenceConnector.readInteger(getApplicationContext(),Master.UpdateRequiredPref , -1)== 1){
                //show recommend update dialog

                showUpdateDialog2();
            }else{
                //force update not required

                //check if all user details are present in database
                CheckAllDetailsPresent();


            }
        }






    }


    private void showUpdateDialog2(){


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //  Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //open play store so that the user can update the app
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.recommend_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //ignore update dialog and continue with the app

                CheckAllDetailsPresent();


            }
        });

        builder.setCancelable(false);
        //Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void CheckAllDetailsPresent() {
        dbHelper.getProfile();
        String fname, lname, password, address, pincode, email, mobileNumber, selectedOrgAbbr, selectedOrgName;

        address= MemberDetails.getAddress();
        fname = MemberDetails.getFname();
        lname = MemberDetails.getLname();
        pincode = MemberDetails.getPincode();
        mobileNumber = MemberDetails.getMobileNumber();

        if(mobileNumber.equals("null")){

            openTalkToUs();
        }else{
            if(address.equals("null")||fname.equals("null")
                    ||lname.equals("null")
                    ||pincode.equals("null")){
                //all details not , redirect to profile page
                ShowStepper("profile");

            }else{
                //all details present , redirect to dashboard
                ShowStepper("dashboard");

            }
        }
    }

    private void Init() {


        // Lifecycle callback method
        Branch branch = Branch.getInstance();

        FlurryAgent.logEvent("branchinitstart" , true);
        branch.initSession(new Branch.BranchReferralInitListener(){
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                FlurryAgent.endTimedEvent("branchinitstart");
                if (error == null) {

                    Map<String, String> articleParams = new HashMap<String, String>();

                    //param keys and values have to be of String type
                    articleParams.put("branchdata", referringParams.toString());
                    FlurryAgent.logEvent("branchdata",articleParams);
                    // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                    // params will be empty if no data found
                    // ... insert custom logic here ...
                    if(debug==1) {
                        Toast.makeText(getApplicationContext(), "branch test " + referringParams.toString(), Toast.LENGTH_LONG).show();
                    }
                    int acceptReferralFailed = SharedPreferenceConnector.readInteger(getApplicationContext() , Master.AcceptReferralFailedPref, 0);

                    try {
                        JSONObject rootJsonObject = new JSONObject(referringParams.toString());
                        //check if the app is opened by clicking on a referral link
                        boolean clickedBranchLink = rootJsonObject.getBoolean("+clicked_branch_link");

                        if(clickedBranchLink){
                            //app opened by clicking on the referral link



                            SharedPreferenceConnector.writeInteger(getApplicationContext() , Master.AcceptReferralFailedPref, 1);

                            //show the stepper for first time launch

                            String phoneNumber = rootJsonObject.getString("phonenumber").substring(2);
                            String email = rootJsonObject.getString("email");
                            String referralCode = rootJsonObject.getString("referralCode");
                            String password = rootJsonObject.getString("token");

                            SharedPreferenceConnector.writeString(getApplicationContext() , Master.ReferralCodePref, referralCode);

                            //check if all the strings we get above are valid
                            if(isValidEmail(email)&&referralCode!=null&&!referralCode.equals("")
                                    &&password!=null&&!password.equals("")&&phoneNumber!=null&&!phoneNumber.equals("")){

                                storeUserDetailsFromBranchData(new String[]{phoneNumber,email,password});

                                //check if network is available
                                if(Master.isNetworkAvailable(getApplicationContext())){
                                    //network is available , execute async task
                                    JSONObject obj = new JSONObject();
                                    obj.put("referralcode",referralCode);

                                    obj.put(Master.VERSION_CHECK_NEW_VERSION_TAG, Extra.version(getApplicationContext()));
                                    new AcceptReferralTask().execute(obj);
                                }else{
                                    //network not available

                                    //this function will check if all details are present in
                                    // local database and redirect to required page
                                    CheckAllDetailsPresent();
                                }


                            }else{
                                //this else would be executed when any of the values of branch data are empty
                                //show an error message
                                //error in referral link
                                Toast.makeText(getApplicationContext(), "An Error occurred, please click on the referral link to try again",Toast.LENGTH_LONG).show();
                                openTalkToUs();
                            }

                        }else if(acceptReferralFailed==1){

                            String rCode = SharedPreferenceConnector.readString(getApplicationContext() , Master.ReferralCodePref, "null");


                            if(rCode.equals("null")){

                            }else{

                                //check if network is available
                                if(Master.isNetworkAvailable(getApplicationContext())){
                                    //network is available , execute async task
                                    JSONObject obj = new JSONObject();
                                    obj.put("referralcode",rCode);

                                    obj.put(Master.VERSION_CHECK_NEW_VERSION_TAG, Extra.version(getApplicationContext()));
                                    new AcceptReferralTask().execute(obj);
                                }else{
                                    //network not available

                                    //this function will check if all details are present in
                                    // local database and redirect to required page
                                    CheckAllDetailsPresent();


                                }
                            }

                        }else{
                            //app not opened by clicking on the referral link

                            //this line stores all profile data(if present) from db to Memberdetails class variables
                            dbHelper.getProfile();




                            //version check api call
                            //check network availability
                            if(Master.isNetworkAvailable(getApplicationContext())){
                                //network is available , execute async task

                                JSONObject obj = new JSONObject();
                                String number = MemberDetails.getMobileNumber();
                                if(number.equals("null")){
                                    //mobile number not present in database

                                }else{
                                    //mobile number present in database
                                    obj.put(Master.VERSION_CHECK_NEW_PHONE_NUMBER_TAG,"91"+number);

                                }

                                obj.put(Master.VERSION_CHECK_NEW_VERSION_TAG, Extra.version(getApplicationContext()));
                                new CheckVersionTask().execute(obj);
                            }else{
                                //network not available
                                //show network not available message
                                //tell user to click on link again
                                Toast.makeText(getApplicationContext(), "Please check your internet connection and click on link again", Toast.LENGTH_LONG).show();
                                openTalkToUs();

                                //*******IMP**********
                                //later give user a try again button within app
                                //**************************

                            }


                            //show stepper in the post execute method of get check version
                            //ShowStepper("");



                        }
                    } catch (JSONException e) {
                        //error parsing JSON , Show an error message here
                        Toast.makeText(getApplicationContext(), "An Error occurred, please try again",Toast.LENGTH_LONG).show();
                        openTalkToUs();

                        e.printStackTrace();
                    }


//                    Log.i("Branch Data ", referringParams.toString());
                } else {
                    //error in branch data
                    // if the user has ad block installed, it may block branch
                    Toast.makeText(getApplicationContext(), "There was an error in the link",Toast.LENGTH_LONG).show();
                    FlurryAgent.onError("1", "Branch Error",  error.getMessage());
                    showAdblockDialog();

//                    Log.i("MyApp", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);

    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }


    public void showAdblockDialog(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //  Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.dialog_show_add_block_message)
                .setTitle(R.string.dialog_show_add_block_tittle);
        builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                CheckAllDetailsPresent();

            }
        });
        builder.setNegativeButton(R.string.dialog_close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //close the app if user clicks close
                finish();
            }
        });

        builder.setCancelable(false);

        //Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public void openTalkToUs(){
        ShowStepper("talktous");
    }


    private void ShowStepper(@NonNull String nextPage){
        if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.STEPPER, Master.DEFAULT_STEPPER)) {

            Intent i = new Intent(getApplicationContext(), StepperActivity.class);
            i.putExtra("redirectto" , nextPage);
            switch(nextPage){
                case "profile":{
                    i.putExtra("showintro" , "true");

                    break;
                }
                case "organisation":{
                    i.putExtra("showintro" , "true");
                    break;
                }
                case "dashboard":{
                    i.putExtra("showintro" , "true");
                    break;
                }
                case "talktous":{

                    break;
                }


            }

            startActivity(i);
            finish();
        } else {
            goToActivity(nextPage);
        }
    }

    private void goToActivity(@NonNull String nextPage) {
        Intent i;
        if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.INTRO, Master.DEFAULT_INTRO)){
            switch(nextPage){
                case "profile":{
                    //go to profile activity
                    i = new Intent(SplashScreenActivity.this, DemoLists.class);
                    i.putExtra("redirectto" , "profile");
                    startActivity(i);
                    finish();
                    break;
                }
                case "organisation":{
                    //change organisation is a fragment and can't be loaded here so,
                    //go to dashboard activity and tell it to load change organisation fragment
                    i = new Intent(SplashScreenActivity.this, DemoLists.class);
                    i.putExtra("redirectto" , "organisation");
                    startActivity(i);
                    finish();
                    break;
                }
                case "dashboard":{
                    //i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                    i = new Intent(SplashScreenActivity.this, DemoLists.class);
                    i.putExtra("redirectto" , "dashboard");
                    startActivity(i);
                    finish();
                    break;
                }
                case "talktous":{
                    i = new Intent(SplashScreenActivity.this, LandingActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
            }

        }else{
            switch(nextPage){
                case "profile":{
                    //go to profile activity
                    i = new Intent(SplashScreenActivity.this, ProfileActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
                case "organisation":{
                    //change organisation is a fragment and can't be loaded here so,
                    //go to dashboard activity and tell it to load change organisation fragment
                    i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                    i.putExtra("redirectto" , "organisation");
                    startActivity(i);
                    finish();
                    break;
                }
                case "dashboard":{
                    //i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                    i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
                case "talktous":{
                    i = new Intent(SplashScreenActivity.this, LandingActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
            }
        }

    }

    private void storeUserDetailsFromBranchData(String[] details){

        //calling this method sets the values in Member details
        //so that when we call addProfile() it writes the new details
        //otherwise without this all other values would be null in MemberDetails other than what we set right now
        //and calling addProfile would set values to null in database taat may be not null before
        dbHelper.getProfile();

        MemberDetails.setEmail(details[1]);
        MemberDetails.setPassword(details[2]);
        MemberDetails.setMobileNumber(details[0]);
        dbHelper.addProfile();


    }


    private void storeUserDetails(JSONObject rootJson){



        //store user details here
        String name, address,lastname , pincode , phonenumber , email , abbr , organisation,profilePicUrl;
        name = address = lastname = pincode = phonenumber = email = abbr = organisation = profilePicUrl = "null";


        try {
            name = rootJson.getString(Master.ACCEPT_REFERRAL_USER_NAME_TAG);
            address  = rootJson.getString(Master.ACCEPT_REFERRAL_USER_ADDRESS_TAG);
            lastname  = rootJson.getString(Master.ACCEPT_REFERRAL_USER_LAST_NAME_TAG);
            pincode  = rootJson.getString(Master.ACCEPT_REFERRAL_PINCODE_TAG);
            phonenumber  = rootJson.getString(Master.ACCEPT_REFERRAL_USER_NUMBER_TAG);
            email  = rootJson.getString(Master.ACCEPT_REFERRAL_USER_EMAIL_TAG);
            if(rootJson.has(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG)&&rootJson.has(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG)) {
                abbr = rootJson.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG);
                organisation = rootJson.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG);
            }

            if(rootJson.has(Master.ACCEPT_REFERRAL_USER_PROFILE_PIC_URL)){
                profilePicUrl  = rootJson.getString(Master.ACCEPT_REFERRAL_USER_PROFILE_PIC_URL);

            }

            MemberDetails.setProfileImageUrl(profilePicUrl);
            MemberDetails.setFname(name);
            MemberDetails.setAddress(address);
            MemberDetails.setLname(lastname);
            MemberDetails.setPincode(pincode);
            MemberDetails.setMobileNumber(phonenumber.substring(2));
            MemberDetails.setEmail(email);
            MemberDetails.setSelectedOrgAbbr(abbr);
            MemberDetails.setSelectedOrgName(organisation);



            //call db helper to store it to database


            //check if selected org is already present
            String selectedOrg[]  = dbHelper.getSelectedOrg(MemberDetails.getMobileNumber());

            //add the details about the user to database
            dbHelper.addProfile();


            if(selectedOrg[0].equals("null")||selectedOrg==null){

                String organisation2Name = "";
                String organisation2Abbr = "";

                //in the new version check api if we give the number of user the response will
                // contain list of organisations he is part of
                //Jsonarray memberships will only be present if we call the new version check api with user mobile number
                if(rootJson.has("memberships")){
                    JSONArray organisationsList= rootJson.getJSONArray("memberships");
                    JSONObject org = organisationsList.getJSONObject(0);
                    organisation2Name = org.getString("organization");
                    organisation2Abbr = org.getString("abbr");

                }

                if(organisation2Name.equals("")||organisation2Name.equals("null")
                        ||organisation2Abbr.equals("")||organisation2Abbr.equals("null")){
                    dbHelper.setSelectedOrg(phonenumber,organisation,abbr);
                }else{
                    dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(),organisation2Name,organisation2Abbr);
                }


            }else{
                //
                dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(),selectedOrg[1],selectedOrg[0]);



            }

            dbHelper.getTable();

//            Log.e("--------", "");



        } catch (JSONException e) {
            e.printStackTrace();
        }




    }

    private void processResponseFromAcceptReferral(JSONObject response) {

        String acceptReferralResponseStatus = "";
        String detailsPresent = "";

        try {
            detailsPresent = response.getString(Master.ACCEPT_REFERRAL_DETAILS_PRESENT);
            acceptReferralResponseStatus  = response.getString(Master.ACCEPT_REFERRAL_RESPONSE_TAG);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //accept referral successful
        if(acceptReferralResponseStatus.equals("success")){

            try {
                String organisationName = response.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG);
                Toast.makeText(getApplicationContext(),"You are now a member of "+organisationName+" this organisation",Toast.LENGTH_SHORT).show();



            } catch (JSONException e) {
                e.printStackTrace();
                //accept referral successful , if you don't see the new organisation in the organisations page please click on the link again
            }



        }else if (acceptReferralResponseStatus.equals("Already a member")){
            //this case would occur if user click on the referral link again
            Toast.makeText(getApplicationContext(),"You are already a member of this organisation",Toast.LENGTH_SHORT).show();


            //check if user details are present
            //redirect to select organisation page

        }else{

        }

        //check if user details are present
        if(detailsPresent.equals("0")){
            //details present
            //redirect to change organisation page
            ShowStepper("organisation");

        }else{
            //details not present
            //show stepper and open profile page to get user data
            ShowStepper("profile");

        }

    }


    private void showRecommendUpdateDialog(int calledFrom){
        from= calledFrom;

        //storing the update required in shared pref , if it is set to 1 ,
        // we show a recommend dialog even if net is not available
        SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref , 1);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //  Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //open play store so that the user can update the app
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.recommend_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //ignore update dialog and continue with the app


                if(from==1){
                    //called from post execute of accept referral
                    processResponseFromAcceptReferral(acceptReferralResponse);

                }else{
                    //called from post execute of version check , Accept referral
                    ShowStepper(goTo);
                }


            }
        });

        builder.setCancelable(false);
        //Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void forceUpdateDialog(){


        //storing the update required in shared pref , if it is set to 2 ,
        // we show a forceupdate dialog even if net is not available
        SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref , 2);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //  Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //open play store so that the user can update the app
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    //this will open play store and show lokacart app
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    //if opening in play store fails open play store in browser
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.force_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //close the app if user clicks close
                finish();
            }
        });

        builder.setCancelable(false);

        //Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public class CheckVersionTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FlurryAgent.logEvent("checkversiontask");
//            System.out.println("pre execute of check version");
            dbHelper.getProfile();
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            Map<String, String> articleParams = new HashMap<String, String>();

            //param keys and values have to be of String type
            articleParams.put("checkversiondatasenttoserver", params[0].toString());
            FlurryAgent.logEvent("checkversiondatasenttoserver",articleParams);
            GetJSON getJson = new GetJSON();
//            System.out.println("check version JSON: " + params[0]);
//            System.out.println("do in background parameters passed -> "+params[0]);
            Master.response = getJson.getJSONFromUrl(Master.getVersionCheckNewURL(), params[0], "POST", false, null, null);
//            System.out.println("Response: " + Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response) {
            FlurryAgent.endTimedEvent("checkversiontask");
            Map<String, String> articleParams = new HashMap<String, String>();
            String response1 = response;
            //there is a max no. of characters that flurry log event supports
            int i=1;
            while (response1.length()>254){
                //articleParams.clear();
                articleParams.put("checkversionresponse--"+i, response1.substring(0,253));
                //FlurryAgent.logEvent("checkversionresponse",articleParams);
                response1 = response1.substring(253);
                i++;
            }

            articleParams.put("checkversionresponse--"+i, response1);
            FlurryAgent.logEvent("checkversionresponse",articleParams);
            //param keys and values have to be of String type

//            System.out.println("check version response" + response);

            if(response.equals("exception")){

                //show error message
                Toast.makeText(getApplicationContext(), "An Error occurred, please click on the referral link to try again",Toast.LENGTH_LONG).show();
                openTalkToUs();


            }else{
                //no exception


                try {
                    JSONObject responseJson = new JSONObject(response);

                    //store user details if present
                    storeUserDetails(responseJson);

                    //either  0,1 or 2
                    //0 -> no update required
                    //1 -> update recommended
                    //2 -> update necessary
                    String updateValue = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_UPDATE_REQUIRED_TAG);

                    switch (updateValue){
                        case "0":{
                            // case -> no update needed
                            //storing the update required in shared pref , if it is set to 0 , no need to update
                            SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref , 0);


                            //response will contain details present flag only if we give the user mobile number if the api call
                            if(responseJson.has(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG)) {


                                //check if all details are present
                                String flag = "";
                                flag = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG);
                                // all details present
                                if (flag.equals("0")) {
                                    ShowStepper("dashboard");
                                } else {
                                    //all details are not present , get details from user
                                    ShowStepper("profile");

                                }
                            }else{
                                //mobile number of user was not present, so open talk to us page
                                openTalkToUs();


                            }
                            break;
                        }
                        case "1":{
                            // case ->  update recommended

                            //if all details are prenent or not will only be
                            // returned if we give the mobile number to the api call
                            if(responseJson.has(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG)) {

                                //check if all details are present
                                String flag = "";
                                flag = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG);
                                // all details present
                                if (flag.equals("0")) {

                                    //
                                    goTo = "dashboard";

                                } else {
                                    //all details are not present , get details from user
                                    goTo = "profile";

                                }

                                showRecommendUpdateDialog(2);
                            }else{
                                //mobile number of user was not present, so open talk to us page
                                openTalkToUs();

                            }

                            break;
                        }
                        case "2":{
                            // case -> update compulsory
                            forceUpdateDialog();
                            break;

                        }

                    }


                } catch (JSONException e) {
                    //show error message
                    Toast.makeText(getApplicationContext(), "An Error occurred, please try again",Toast.LENGTH_LONG).show();
                    openTalkToUs();

                    e.printStackTrace();
                }

            }

//            Log.d("check apk version respo" , response);
//            System.out.println("post execute of check version");
            dbHelper.getProfile();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        FlurryAgent.onStartSession(this,getString(R.string.flurry_key));
        FlurryAgent.onPageView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FlurryAgent.onEndSession(this);
    }

    public class AcceptReferralTask extends AsyncTask<JSONObject, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FlurryAgent.logEvent("acceptreferraltaskstart",true);
//            System.out.println("pre execute of accept referral");
            dbHelper.getProfile();

        }

        @Override
        protected String doInBackground(JSONObject... params) {
            Map<String, String> articleParams = new HashMap<String, String>();

            //param keys and values have to be of String type
            articleParams.put("acceptreferraldatasenttoserver", params[0].toString());
            FlurryAgent.logEvent("acceptreferraldatasenttoserver",articleParams);
            GetJSON getJson = new GetJSON();
//            System.out.println("Accept referral JSON: " + params[0]);
//            System.out.println("do in background of accept referral -> parameters passed" +params[0]);
            Master.response = getJson.getJSONFromUrl(Master.getAcceptReferralURL(), params[0], "POST", false, null, null);


//            System.out.println("Response: " + Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response) {
            FlurryAgent.endTimedEvent("acceptreferraltaskstart");

            Map<String, String> articleParams = new HashMap<String, String>();
            String response1 = response;
            //there is a max no. of characters that flurry log event supports
            int i=1;
            while (response1.length()>254){
                //articleParams.clear();
                articleParams.put("acceptreferralresponse--"+i, response1.substring(0,253));
                //FlurryAgent.logEvent("acceptreferralresponse",articleParams);
                response1 = response1.substring(253);
                i++;
            }
            //articleParams.clear();
            articleParams.put("acceptreferralresponse--"+i, response1);
            FlurryAgent.logEvent("acceptreferralresponse",articleParams);

//            System.out.println("Accept referral response" + response);

            if(response.equals("exception"))
            {

                SharedPreferenceConnector.writeInteger(getApplicationContext() , Master.AcceptReferralFailedPref, 1);


                //an exception occurred while communicating with the server for accepting the referral
                //save referral code and give user the option to try again
                //also tell user to click on the link again later

                Toast.makeText(getApplicationContext(), "An Error occurred, please try again",Toast.LENGTH_LONG).show();
                //TODO
                //call check userdetals
                openTalkToUs();


            }
            else
            {

                SharedPreferenceConnector.writeInteger(getApplicationContext() , Master.AcceptReferralFailedPref, 0);

                try {
                    acceptReferralResponse = new JSONObject(response);
                    String responseValue = acceptReferralResponse.getString("response");

                    //check if updating the app is mandatory
                    String mandatory = acceptReferralResponse.getString(Master.ACCEPT_REFERRAL_UPDATE_MANDATORY_TAG);


                    //user details will only be present if the response of accept referral is success
                    if(responseValue.equals("success")) {

                        //store user details
                        storeUserDetails(acceptReferralResponse);

                    }else if(responseValue.equals("Already a member")){

                        //all user details would be returned even in the case of already a member
                        storeUserDetails(acceptReferralResponse);

                    }

                    String flag = "";
                    flag = acceptReferralResponse.getString(Master.ACCEPT_REFERRAL_DETAILS_PRESENT);

                    //mandatory = 2 means updating the app is mandatory
                    //mandatory = 1 means updating the app is recommended
                    //mandatory = 0 means updating the app is not required
                    if(mandatory.equals("2")){
                        //store user details and show the dialog to update the app

                        forceUpdateDialog();

                    }else if(mandatory.equals("1")){
                        showRecommendUpdateDialog(1);
                    }else{
                        //storing the update required in shared pref , if it is set to 0, no update needed
                        SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref , 0);

                        processResponseFromAcceptReferral(acceptReferralResponse);

                    }



                } catch (JSONException e) {
                    //show error message

                    Toast.makeText(getApplicationContext(), "An Error occurred, please try again",Toast.LENGTH_LONG).show();
                    openTalkToUs();

                    e.printStackTrace();
                }
            }


//            Log.d("Accept referraresponse" , response);
//            System.out.println("post execute of accept referral");
            dbHelper.getProfile();

        }
    }

}