package com.mobile.ict.cart.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alertdialogpro.AlertDialogPro;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.activity.MapActivity;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

/**
 * Created by vish on 21/3/16.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener{

    View profileFragmentView, editMobileNumberDialogView;
    TextInputEditText eFName, eLName, eAddress, ePincode, eMobileNumber, eOtp;
    CardView cardView2;
    ImageView ivEdit, ivDone, ivMobileNumberDone, iVProfileEdit,iVProfile;
    TextView tMobileNumber, tName, tEmail, tAddress, tPincode;
    String localFName, localLName, localAddress, localPincode, localMobileNumber;
    AlertDialogPro changeMobileNumberAlert;
    OTPCountDownTimer countDownTimer;
    String otp_check;
    long session;
    File file, file1;
    String tempImage;
    final int REQUEST_GALLERY = 2, REQUEST_CAMERA = 1;
    DBHelper dbHelper;
    Button bPositive, bNegative, bNeutral;
    ImageButton bLocation;

    boolean isVisible = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActivity().setTitle(R.string.title_fragment_profile);

        setHasOptionsMenu(true);


        Master.getMemberDetails(getActivity().getApplicationContext());

        if(!Master.isNetworkAvailable(getActivity()))
        {
            profileFragmentView = inflater.inflate(R.layout.no_internet_layout, container, false);
        }
        else
        {
            profileFragmentView = inflater.inflate(R.layout.fragment_profile, container, false);

            init();
            assign();
        }


        return profileFragmentView;
    }



    @Override
    public void onResume() {
        super.onResume();
        Master.getMemberDetails(getActivity().getApplicationContext());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Master.getMemberDetails(getActivity().getApplicationContext());
        System.out.println(MemberDetails.getFname() + "---------" + MemberDetails.getLname() + "---------" + MemberDetails.getAddress() + "---------" + MemberDetails.getPincode());

    }

    void init()
    {
        dbHelper = new DBHelper(getActivity());

        cardView2 = (CardView) profileFragmentView.findViewById(R.id.card_view2);
        cardView2.setVisibility(View.GONE);

        eFName = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfileFName);
        eLName = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfileLName);
        eMobileNumber = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfileMobileNumber);
        ePincode = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfilePincode);
        eAddress = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfileAddress);
        iVProfileEdit = (ImageView)profileFragmentView.findViewById(R.id.tEditProdilePhoto);
        iVProfileEdit.setOnClickListener(this);
        iVProfile = (ImageView)profileFragmentView.findViewById(R.id.iProdfilePhoto);
        /*eAddress.setScroller(new Scroller(getActivity()));
        eAddress.setMaxLines(4);
        eAddress.setVerticalScrollBarEnabled(true);
        eAddress.setMovementMethod(new ScrollingMovementMethod());*/

        tName = (TextView) profileFragmentView.findViewById(R.id.tProfileName);
        tEmail = (TextView) profileFragmentView.findViewById(R.id.tProfileEmail);
        tAddress = (TextView) profileFragmentView.findViewById(R.id.tProfileAddress);
        tPincode = (TextView) profileFragmentView.findViewById(R.id.tProfilePincode);
        tMobileNumber = (TextView) profileFragmentView.findViewById(R.id.tProfileMobileNumber);

        // Location button
        bLocation = (ImageButton)profileFragmentView.findViewById(R.id.bMyLocation);
        bLocation.setOnClickListener(this);

        ivEdit = (ImageView) profileFragmentView.findViewById(R.id.ivEdit);
        ivEdit.setOnClickListener(this);

        ivDone = (ImageView) profileFragmentView.findViewById(R.id.ivDone);
        ivDone.setOnClickListener(this);



        ivMobileNumberDone = (ImageView) profileFragmentView.findViewById(R.id.ivMobileNumberDone);
        ivMobileNumberDone.setOnClickListener(this);
    }

    void assign()
    {
        isVisible = false;

        tName.setText(MemberDetails.getFname() + " " + MemberDetails.getLname());
        tEmail.setText(MemberDetails.getEmail());
        tMobileNumber.setText(MemberDetails.getMobileNumber());
        tAddress.setText(MemberDetails.getAddress());
        tPincode.setText(MemberDetails.getPincode());

        eFName.setText(MemberDetails.getFname());
        eLName.setText(MemberDetails.getLname());
        eMobileNumber.setText(MemberDetails.getMobileNumber());
        eAddress.setText(MemberDetails.getAddress());
        ePincode.setText(MemberDetails.getPincode());

        localFName = MemberDetails.getFname();
        localLName = MemberDetails.getLname();
        localAddress = MemberDetails.getAddress();
        localPincode = MemberDetails.getPincode();
        localMobileNumber = MemberDetails.getMobileNumber();
        Glide.with(getActivity()).load(MemberDetails.getProfileImageUrl()).asBitmap().centerCrop().into(new BitmapImageViewTarget(iVProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                iVProfile.setImageDrawable(circularBitmapDrawable);
            }
        });

//        Glide.with(getActivity()).load(MemberDetails.getProfileImageUrl())
//                .thumbnail(0.5f)
//                .crossFade()
//                .placeholder(R.drawable.profile_placeholder)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(iVProfile);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onClick(View v) {

        Master.hideSoftKeyboard(getActivity());
        switch (v.getId())
        {
            case R.id.ivEdit:
                if(!(MemberDetails.getProfileImageUrl().equals("null")||MemberDetails.getProfileImageUrl().equals(""))){


                    try {
                        Glide.with(getActivity()).load(MemberDetails.getProfileImageUrl()).asBitmap().centerCrop().into(new BitmapImageViewTarget(iVProfileEdit) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                iVProfileEdit.setImageDrawable(circularBitmapDrawable);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                if(isVisible)
                {
                    isVisible = false;
                    cardView2.setVisibility(View.GONE);
                }
                else
                {
                    isVisible = true;
                    cardView2.setVisibility(View.VISIBLE);
                    eMobileNumber.setText(MemberDetails.getMobileNumber());
                }
                break;

            case R.id.ivDone:
                save();
                break;
            case R.id.tEditProdilePhoto:{


                uploadImage();
                break;
            }
            case R.id.ivMobileNumberDone:
                if(eMobileNumber.getText().toString().trim().length() != 10)
                    Toast.makeText(getActivity(), R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();
                else if(eMobileNumber.getText().toString().trim().charAt(0) == '0')
                    Toast.makeText(getActivity(), R.string.toast_dont_prefix_zero_to_the_mobile_number, Toast.LENGTH_LONG).show();
                else if(localMobileNumber.equals(eMobileNumber.getText().toString().trim()))
                    Toast.makeText(getActivity(), R.string.toast_no_changes_to_save, Toast.LENGTH_LONG).show();
                else
                {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("phonenumber_old","91"+ MemberDetails.getMobileNumber());
                        obj.put("phonenumber_new","91" +eMobileNumber.getText().toString().trim());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new MobileNumberVerifyTask(2).execute(obj);
                }
                break;
            case R.id.bMyLocation:
               /* AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);
                builder.setCancelable(false).setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }) */
                Intent intent = new Intent(getActivity(), MapActivity.class);
                Log.v("Intent","Here");
                startActivity(intent);
                break;
        }
    }

    void selectProfilePic(){


    }

    private void uploadImage()
    {




        final CharSequence[] options = {
//                "Take Photo",
                "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    tempImage = "temp" + new Random().nextInt(1000000) + ".jpg";
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), tempImage);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/jpg");
                    startActivityForResult(intent, REQUEST_GALLERY);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK)
        {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals(tempImage)) {
                        f = temp;
                        break;
                    }
                }

                try {
                    final Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    //bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
                    bitmap = readBitmap(Uri.fromFile(f), getActivity(), 2);
                    //bitmap = ImageUtils.decodeSampledBitmap(EditProductActivity.this, Uri.fromFile(f));



                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            else if (requestCode == REQUEST_GALLERY)
            {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals(tempImage)) {
                        f = temp;
                        break;
                    }
                }
                Uri selectedImageURI = data.getData();
                if(selectedImageURI != null) {
                    try {
                        Bitmap bitmap;
                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                        //bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
                        bitmap = readBitmap(selectedImageURI, getActivity(), 2);
                        bitmap = getCroppedBitmap(bitmap);

                        //iVProfileEdit.setScaleType(ImageView.ScaleType.CENTER_CROP);


                        File imageFile = new File(getRealPathFromURI(selectedImageURI));
                        long size = imageFile.length()/1024;
                        if(size<1025){
                            iVProfileEdit.setImageBitmap(bitmap);
                            new UploadImageTask(imageFile.getAbsolutePath(),bitmap).execute();
                        }else{
                            Toast.makeText(getActivity(),"Select an image which is less than 1Mb", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                       Toast.makeText(getActivity(), "error in async task", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    //------------function to read an image and scale it down--------------------------------
    //---------------------------------------------------------------------------------------
    public static Bitmap readBitmap(Uri selectedImage, Context context, int scalValue) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = scalValue;
        AssetFileDescriptor fileDescriptor =null;
        try {
            fileDescriptor = context.getContentResolver().openAssetFileDescriptor(selectedImage, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally{
            try {
                bm = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);
                fileDescriptor.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bm;
    }

    private String getRealPathFromURI(Uri contentURI) {
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    class UploadImageTask extends AsyncTask<Void, String, String>
    {
        ProgressDialog pd;
        String path;

        Bitmap bitmap;
        Boolean isCaptured;String imagePath;

        UploadImageTask(String path, Bitmap bitmap)
        {
            this.bitmap = bitmap;
            this.path = path;


        }
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.pd_uploading_image));
            pd.setCancelable(false);
            pd.show();

            // System.out.println(path);

            try {
                file = new File(path);
            }catch(Exception e){
                e.printStackTrace();
            }

            //System.out.println(file.getAbsolutePath());
            // System.out.println(file.getParent());

            imagePath = file.getParent() + "/" + "myImage" + new Random().nextInt(1000000) + ".jpg";


            file1 = new File(imagePath);
            try
            {
                Master.copyFile(file, file1);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String response;
            response = Master.okhttpUpload(file, Master.getUploadImageURL(MemberDetails.getMobileNumber()),
                    Master.IMAGE_FILE_TYPE, "91"+MemberDetails.getMobileNumber(), MemberDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
                JSONObject responseObject = new JSONObject(message);
                message=responseObject.optString("response");
                //System.out.println("message"+message.toString());


                if(message.equals("Image upload successful"))
                {
                    Toast.makeText(getActivity(),
                            R.string.toast_image_has_been_uploaded_successfully, Toast.LENGTH_LONG).show();
                    Master.clearBitmap(bitmap);
                    if(responseObject.has("url")) {
                        String imageUrl = responseObject.getString("url");
                        Glide.with(getActivity()).load(imageUrl)
                                .thumbnail(0.5f)
                                .crossFade()
                                .placeholder(R.drawable.profile_placeholder)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(iVProfile);
                        MemberDetails.setProfileImageUrl(imageUrl);

                        Glide.with(getActivity()).load(MemberDetails.getProfileImageUrl()).asBitmap().centerCrop().into(new BitmapImageViewTarget(iVProfile) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                iVProfile.setImageDrawable(circularBitmapDrawable);
                            }
                        });
                        dbHelper.addProfile();
                    }


                }
                else
                {
                    Toast.makeText(getActivity(),
                            R.string.alert_cannot_connect_to_the_server, Toast.LENGTH_LONG).show();

                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(getActivity(),
                        R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
            }
        }
    }
//---------------------End of Upload Image Asynctask-------------------------------------------------


    void save()
    {
        if(eFName.getText().toString().trim().equals(""))
            eFName.setError(getString(R.string.error_required));
        else if(eLName.getText().toString().trim().equals(""))
            eLName.setError(getString(R.string.error_required));
        else if(eAddress.getText().toString().trim().equals(""))
            eAddress.setError(getString(R.string.error_required));
        else if(ePincode.getText().toString().trim().equals(""))
            ePincode.setError(getString(R.string.error_required));
        else if(ePincode.getText().toString().trim().length() != 6)
            Toast.makeText(getActivity(), R.string.toast_please_enter_a_valid_six_digit_pincode, Toast.LENGTH_LONG).show();
        else if (eFName.getText().toString().trim().equals(localFName) && eLName.getText().toString().trim().equals(localLName)
                && eAddress.getText().toString().trim().equals(localAddress) && ePincode.getText().toString().trim().equals(localPincode))
        {
            Toast.makeText(getActivity(), R.string.toast_no_changes_to_save, Toast.LENGTH_LONG).show();
        }
        else
        {
            JSONObject jsonObject = new JSONObject();
            try
            {
                jsonObject.put(Master.FNAME, eFName.getText().toString().trim());
                jsonObject.put(Master.LNAME, eLName.getText().toString().trim());
                jsonObject.put(Master.ADDRESS, eAddress.getText().toString().trim());
                jsonObject.put(Master.PINCODE, ePincode.getText().toString().trim());
                jsonObject.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());

                new EditDetailsTask().execute(jsonObject);
            }
            catch (JSONException e)
            {
                Toast.makeText(getActivity(), R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
            }
        }
    }

    void editMobileNumber()
    {
        AlertDialogPro.Builder builder = new AlertDialogPro.Builder(getActivity());
        builder.setCancelable(false);
        editMobileNumberDialogView = getActivity().getLayoutInflater().inflate(R.layout.change_mobile_number_box, null);
        builder.setView(editMobileNumberDialogView);
        builder.setTitle(R.string.title_dialog_change_mobile_number);
        builder.setPositiveButton(R.string.button_confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNeutralButton(R.string.button_get_OTP, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        eOtp = (TextInputEditText) editMobileNumberDialogView.findViewById(R.id.eOtp1);
        changeMobileNumberAlert = builder.create();
        changeMobileNumberAlert.show();

        bPositive = changeMobileNumberAlert.getButton(AlertDialogPro.BUTTON_POSITIVE);
        bNegative = changeMobileNumberAlert.getButton(AlertDialogPro.BUTTON_NEGATIVE);
        bNeutral = changeMobileNumberAlert.getButton(AlertDialogPro.BUTTON_NEUTRAL);
        bNeutral.setVisibility(View.GONE);

        bPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long session2 = System.currentTimeMillis();

                if(session2<session)
                {
                    if(eOtp.getText().toString().trim().isEmpty())
                    {
                        Toast.makeText(getActivity(),"Please " + getString(R.string.toast_enter_otp_received), Toast.LENGTH_LONG).show();
                    }
                    else if(eOtp.getText().toString().trim().equals(otp_check))
                    {
                        changeMobileNumberAlert.dismiss();
                        countDownTimer.cancel();

                        JSONObject jsonObject = new JSONObject();
                        try
                        {
                            jsonObject.put("phonenumber", "91" + MemberDetails.getMobileNumber());
                            jsonObject.put("phonenumber_new", "91"+eMobileNumber.getText().toString().trim());

//                            System.out.println("editing mobile number-------------"+jsonObject.toString());

                            new EditMobileNumberTask(getActivity(), MemberDetails.getMobileNumber(),
                                    eMobileNumber.getText().toString().trim()).execute(jsonObject);
                        }
                        catch (JSONException e)
                        {
                            Toast.makeText(getActivity(), R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(getActivity(),R.string.toast_Please_enter_correct_OTP, Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    eOtp.setText("");
                    Toast.makeText(getActivity(),R.string.toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();
                }
            }
        });


        bNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeMobileNumberAlert.dismiss();
            }
        });

        bNeutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject obj = new JSONObject();
                try {
                    obj.put("phonenumber_old","91"+ MemberDetails.getMobileNumber());
                    obj.put("phonenumber_new","91" +eMobileNumber.getText().toString().trim());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                new MobileNumberVerifyTask(1).execute(obj);
            }
        });

    }


    //------------------------------ change mobile number task ----------------------------------



    public class MobileNumberVerifyTask extends AsyncTask<JSONObject, String, String> {

        int category;

        public MobileNumberVerifyTask(int cat)
        {
            category = cat;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_sending_data_to_server), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            Master.getJSON = new GetJSON();
//            System.out.println("----Password: " + MemberDetails.getPassword());
//            System.out.println("-----Get otp JSON: " + params[0]);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getNumberVerifyURL(), params[0], "POST", true,
                    MemberDetails.getEmail(), MemberDetails.getPassword());
//            System.out.println("-----Get otp response: " + Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String s) {
            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(ProfileFragment.this.isAdded())
            {
                if(s.equals("exception"))
                {
                    Material.alertDialog(getActivity(), getActivity().getString(R.string.alert_cannot_connect_to_the_server), "OK");
                }
                else
                {
                    try
                    {
                        Master.responseObject = new JSONObject(s);
                        String otp_val =  Master.responseObject.getString("otp");
                        String text =  Master.responseObject.getString("text");
                        if(!otp_val.equals("null")&&!text.equals("null"))
                        {
                            Toast.makeText(getActivity(),R.string.toast_OTP_has_sent_to_your_mobile_sms,Toast.LENGTH_LONG).show();

                            otp_check = otp_val;
                            session = System.currentTimeMillis();
                            countDownTimer = new OTPCountDownTimer(600000, 1000);
                            countDownTimer.start();

                            session = session+600000; // 10 minutes

                            if(category == 2)
                                editMobileNumber();
                            else
                            {
                                bNeutral.setVisibility(View.GONE);
                                bPositive.setVisibility(View.VISIBLE);
                            }
                        }
                        else
                        {
                            if(otp_val.equals("null")&&text.equals("Phone number entered already exists."))
                            {
                                Toast.makeText(getActivity(),R.string.toast_validation_Mobile_Number_exists, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    catch (JSONException e)
                    {
                        Toast.makeText(getActivity(), R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }




    public class EditMobileNumberTask extends AsyncTask<JSONObject, String, String> {

        Context context;
        String oldNum, newNum;

        public EditMobileNumberTask(Context context, String oldNum, String newNum) {
            // TODO Auto-generated constructor stub
            this.context = context;
            this.newNum = newNum;
            this.oldNum = oldNum;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_sending_data_to_server), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            Master.getJSON = new GetJSON();
//            System.out.println("edit number URL: " + Master.getChangeNumberURL());
//            System.out.println("edit number JSON: " + params[0]);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getChangeNumberURL(), params[0], "POST", true,
                    MemberDetails.getEmail(), MemberDetails.getPassword());
//            System.out.println("Change mobile number response---:" + Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String s) {

            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(ProfileFragment.this.isAdded())
            {
                if(s.equals("exception"))
                {
                    Material.alertDialog(getActivity(), getActivity().getString(R.string.alert_cannot_connect_to_the_server), "OK");
                }
                else
                {
                    try
                    {
                        Master.responseObject = new JSONObject(s);

                        if(Master.responseObject.getString(Master.STATUS).equals("success"))
                        {
                            Toast.makeText(getActivity(),R.string.toast_Your_mobile_number_upadted_successfully,Toast.LENGTH_LONG).show();
                            dbHelper.changeMobileNumber(oldNum, newNum);
                            tMobileNumber.setText(newNum);
                            localMobileNumber = newNum;
                            MemberDetails.setMobileNumber(newNum);
                            DashboardActivity.updateNavHeader();
                        }
                        else if(Master.responseObject.getString(Master.STATUS).equals("user phone number not present"))
                        {
                            Toast.makeText(getActivity(),R.string.toast_Mobile_Number_does_not_exists,Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), R.string.alert_cannot_connect_to_the_server, Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        Toast.makeText(getActivity(), R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

//----------------------- OTP Count Down Timer------------------------------------------------------

    public class OTPCountDownTimer extends CountDownTimer
    {

        public OTPCountDownTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }

        @Override
        public void onFinish()
        {
//            Log.e("Profile frag ----", "onFinish()");
            if(ProfileFragment.this.isAdded())
            {
                if(editMobileNumberDialogView != null)
                {
                    eOtp.setText("");
                    bPositive.setVisibility(View.GONE);
                    bNeutral.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(),R.string.toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();
                }
            }
        }
        @Override
        public void onTick(long millisUntilFinished)
        {
        }
    }


    class EditDetailsTask extends AsyncTask<JSONObject, String, String>
    {
        @Override
        protected void onPreExecute() {
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_sending_data_to_server), true);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            Master.getJSON = new GetJSON();
//            System.out.println("URL: " + Master.getEditProfileURL());
//            System.out.println("JSON: " + params[0]);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getEditProfileURL(), params[0], "POST", true,
                    MemberDetails.getEmail(), MemberDetails.getPassword());
//            System.out.println(Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String s) {

            if(ProfileFragment.this.isAdded())
            {
                if(s.equals("exception"))
                {
                    Material.alertDialog(getActivity(), getActivity().getString(R.string.alert_cannot_connect_to_the_server), "OK");
                }
                else
                {
                    try {
                        Master.responseObject = new JSONObject(s);
                    }
                    catch (Exception e)
                    {
//                        Log.e("Prof frag", "JSON prob");
                    }
                    try {
                        if(Master.responseObject.getString(Master.RESPONSE).equals("Success"))
                        {
                            Toast.makeText(getActivity(), R.string.toast_profile_details_updated_successfully, Toast.LENGTH_LONG).show();

                            MemberDetails.setFname(eFName.getText().toString().trim());
                            MemberDetails.setLname(eLName.getText().toString().trim());
                            MemberDetails.setAddress(eAddress.getText().toString().trim());
                            MemberDetails.setPincode(ePincode.getText().toString().trim());

//                            System.out.println("edit task------------" + MemberDetails.getFname() + "---------" + MemberDetails.getLname() + "---------" + MemberDetails.getAddress() + "---------" + MemberDetails.getPincode());

                            dbHelper.updateProfile(MemberDetails.getMobileNumber());

                            assign();
                            DashboardActivity.updateNavHeader();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), R.string.alert_cannot_connect_to_the_server, Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        Toast.makeText(getActivity(), R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                    }
                }
            }
            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();
        }
    }
}
