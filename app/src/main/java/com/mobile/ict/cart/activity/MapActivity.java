package com.mobile.ict.cart.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobile.ict.cart.R;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.util.List;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,LocationListener,GoogleMap.OnCameraChangeListener {

    private static final String TAG = MapActivity.class.getSimpleName();
    private static final String key = "saveLocation";
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Button bSave;
    private ImageView google_marker;
    private EditText locationBox;
    private ImageButton marker,mylocation;
    private  PlaceAutocompleteFragment autocompleteFragment;
    private  LocationRequest mLocationRequest;
    //private EditText eText;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Context context;
    private ConnectivityManager cm;

    private GoogleMap mMap;
    private LatLng MyLocation=null;
    static final int ZOOM_LEVEL = 15;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        View view = findViewById(R.id.map);

        view.setOnTouchListener(new View.OnTouchListener() {
                                    public boolean onTouch(View view, MotionEvent event) {
                                        if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                                            Log.d("TouchTest", "Touch down");
                                            return true;
                                        } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                                            Log.d("TouchTest", "Touch up");
                                            return false;
                                        }
                                        return false;
                                    }

                                });



        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
         context = this;
        cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);



        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                MyLocation = place.getLatLng();


                String lable = place.getAddress().toString();

                mMap.clear();
                String locationAddress = getAddress();
                mMap.moveCamera(CameraUpdateFactory.newLatLng(MyLocation));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL));

                System.out.println(locationAddress);
                locationBox.setText(locationAddress);

    }

    @Override
    public void onError(Status status) {
        // TODO: Handle the error.
        Log.i(TAG, "An error occurred: " + status);
    }
});

      //  marker = (ImageButton)findViewById(R.id.marker);
     //   mylocation = (ImageButton)findViewById(R.id.mylocation);

        bSave = (Button)findViewById(R.id.bSave);
        locationBox = (EditText)findViewById(R.id.locationBox);
        locationBox.setVisibility(View.GONE);
        locationBox.setFocusable(false);
        locationBox.setText("No Location");
        google_marker = (ImageView)findViewById(R.id.location_marker);
        google_marker.setVisibility(View.INVISIBLE);
      //  bSearch = (Button)findViewById(R.id.bSearch);
     //   eText = (EditText)findViewById(R.id.searchView1); 
        if(checkPlayServices())
            buildGoogleApiClient();

        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mMap!=null) {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    String location = mMap.getCameraPosition().target.latitude + " " + mMap.getCameraPosition().target.longitude;
                    editor.putString(key, location);
                    editor.commit();
                    onBackPressed();
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        Log.v("My location", String.valueOf(MyLocation));
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        checkPlayServices();
        Log.v("My location", String.valueOf(MyLocation));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnCameraChangeListener(this);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                Log.e("TAG", mMap.getCameraPosition().target.toString());
            }
        });
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener(){
            @Override
            public boolean onMyLocationButtonClick(){
                if (ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.

                    return false;
                }
                mLastLocation = LocationServices.FusedLocationApi
                        .getLastLocation(mGoogleApiClient);
                MyLocation = new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude());
                String locationAddress = getAddress();
                System.out.println(locationAddress);
                locationBox.setText(locationAddress);
               // setMarkerPosition();
                return false;

            }
        });
       // mMap.setPadding(0,1000,600,0);

        String locationAddress = getAddress();
        System.out.println(locationAddress);
        locationBox.setText(locationAddress);
        setMarkerPosition();
    }
    public void setMarkerPosition(){
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected) {
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(MyLocation.latitude, MyLocation.longitude, 3);

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null) {
                String lable = addresses.get(0).getAddressLine(0);
                mMap.clear();

           /* Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(MyLocation)
                    .title(lable)
                    .draggable(true)); */

                mMap.moveCamera(CameraUpdateFactory.newLatLng(MyLocation));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL));
                //  mMap.setOnMarkerDragListener(this);
                Log.v("zoom level", String.valueOf(mMap.getMaxZoomLevel()));

            }
        }
        else{
            Toast.makeText(this,"Could not fetch location please check your internet connection",Toast.LENGTH_LONG).show();
        }
    }
    public String getAddress(){
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected) {
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(MyLocation.latitude, MyLocation.longitude, 3);

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null) {
                System.out.println(addresses.get(0));
                System.out.println(addresses.get(1));
                System.out.println(addresses.get(2));
                String lable = addresses.get(0).getAddressLine(0);
                return lable;
            }
        }
        else{
            Toast.makeText(this,"Could not fetch location please check your internet connection",Toast.LENGTH_LONG).show();
            return "No Location";
        }
        return "No Location";
    }
    private void getUserLocation() throws IOException {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            return;
        }
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String location = sharedPreferences.getString(key,null);
            //  Log.v("latitude and longitude",location);
            if(location==null)
                MyLocation = new LatLng(latitude,longitude);
            else
                MyLocation = new LatLng(Double.parseDouble(location.split(" ",2)[0]),Double.parseDouble(location.split(" ",2)[1]));

            Log.v("latitude and longitude", latitude + " " + longitude);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);


        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        if(MyLocation!=null) {
            mapFragment.getMapAsync(this);
            google_marker.setVisibility(View.VISIBLE);
            locationBox.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        return;
        }
            lm.addGpsStatusListener(new android.location.GpsStatus.Listener() {
                public void onGpsStatusChanged(int event) {
                  //  System.out.println("Location setting changed");
                    switch (event) {
                        case GpsStatus.GPS_EVENT_STARTED:
                            System.out.println("Location setting on");
                            break;
                        case GpsStatus.GPS_EVENT_STOPPED:
                            // do your tasks
                            break;
                    }
                }
            });

        try {
            getUserLocation();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onLocationChanged(Location location) {
      //  Log.v("Update","Its happening");
       // mLastLocation = location;
        try {
            getUserLocation();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onConnectionSuspended(int i) {

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }


    private boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }
    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        createLocationRequest();

    }
    protected void createLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    MapActivity.this, 1000);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });

    }
    protected void startLocationUpdates() {
    //    Log.v("Update","First Time");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
           return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    protected void search(List<Address> addresses) {

        Address address = (Address) addresses.get(0);
        MyLocation = new LatLng(address.getLatitude(), address.getLongitude());

        String lable = addresses.get(0).getAddressLine(0);
      /*  addressText = String.format(
                "%s, %s",
                address.getMaxAddressLineIndex() > 0 ? address
                        .getAddressLine(0) : "", address.getCountryName());*/

        MarkerOptions markerOptions = new MarkerOptions();

        markerOptions.position(MyLocation).title(lable);
        // markerOptions.title(addressText);
        markerOptions.draggable(true);
        mMap.clear();
        mMap.addMarker(markerOptions);
        //mMap.setOnMarkerDragListener(this);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(MyLocation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL));
        // locationTv.setText("Latitude:" + address.getLatitude() + ", Longitude:"
        //         + address.getLongitude());


    }

    @Override
    public void onCameraChange(CameraPosition position){
        MyLocation = position.target;
        Log.v("My location", String.valueOf(MyLocation));
        mMap.clear();
        if(mMap!=null) {
            MyLocation = mMap.getCameraPosition().target;
            String locationAddress = getAddress();
            System.out.println(locationAddress);
            locationBox.setText(locationAddress);
        }
    }

}
