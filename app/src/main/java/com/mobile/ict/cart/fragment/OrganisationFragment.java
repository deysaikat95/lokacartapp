package com.mobile.ict.cart.fragment;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alertdialogpro.AlertDialogPro;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mobile.ict.cart.LokacartApplication;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Organisations;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.adapter.OrganisationAdapter;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.JSONDataHelper;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vish on 21/3/16.
 */
public class OrganisationFragment extends Fragment implements View.OnClickListener{

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    View changeOrganisationFragmentView;
    Button bChangeOrganisation, bLeaveOrganisation;
    TextView tOrganisationZero, tNoData;
    ImageView ivNoData;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    protected LayoutManagerType mCurrentLayoutManagerType;
    DBHelper dbHelper;

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private Tracker mTracker;
    private static final String TAG = "OrganisationFragment";
    String name = "Contact";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActivity().setTitle(R.string.title_fragment_change_organisation);

        setHasOptionsMenu(true);

        // Obtain the shared Tracker instance.
        LokacartApplication application = (LokacartApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Fragment~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        if(!Master.isNetworkAvailable(getActivity()))
        {
            changeOrganisationFragmentView = inflater.inflate(R.layout.no_internet_layout, container, false);
        }
        else
        {
            changeOrganisationFragmentView = inflater.inflate(R.layout.fragment_organisation, container, false);

            dbHelper = new DBHelper(getActivity());
            mRecyclerView = (RecyclerView) changeOrganisationFragmentView.findViewById(R.id.organisationRecyclerView);
            tOrganisationZero = (TextView) changeOrganisationFragmentView.findViewById(R.id.tOrganisationZero);
            bChangeOrganisation = (Button) changeOrganisationFragmentView.findViewById(R.id.bChangeOrganisation);
            bLeaveOrganisation = (Button) changeOrganisationFragmentView.findViewById(R.id.bLeaveOrganisation);
            ivNoData = (ImageView) changeOrganisationFragmentView.findViewById(R.id.ivNoData);
            ivNoData.setVisibility(View.GONE);
            tNoData = (TextView) changeOrganisationFragmentView.findViewById(R.id.tNoData);
            tNoData.setVisibility(View.GONE);

            mRecyclerView.setHasFixedSize(true);

            mLayoutManager = new LinearLayoutManager(getActivity());
            mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
            if (savedInstanceState != null) {
                mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                        .getSerializable(KEY_LAYOUT_MANAGER);
            }
            setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

            bChangeOrganisation.setOnClickListener(this);
            bLeaveOrganisation.setOnClickListener(this);
        }

        return changeOrganisationFragmentView;
    }


    @Override
    public void onResume() {
        super.onResume();
        Master.getMemberDetails(getActivity());

        // Obtain the shared Tracker instance.
        LokacartApplication application = (LokacartApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Fragment~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        if(Master.isNetworkAvailable(getActivity()))
        {
            if(bChangeOrganisation != null)
            {
                bChangeOrganisation.setVisibility(View.GONE);
                bLeaveOrganisation.setVisibility(View.GONE);

                try
                {
                    JSONObject obj = new JSONObject();
                    obj.put(Master.MOBILENUMBER,"91"+ MemberDetails.getMobileNumber());
                    obj.put(Master.PASSWORD, MemberDetails.getPassword());
                    new GetOrganisationsTask().execute(obj);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.bLeaveOrganisation:

                AlertDialogPro.Builder builder = new AlertDialogPro.Builder(getActivity());
                builder.setCancelable(false);
                builder.setMessage(R.string.alert_Do_you_want_terminate_this_organization_memebership);
                builder.setTitle(Organisations.organisationList.get(OrganisationAdapter.getLastCheckedPos()).getName());

                builder.setPositiveButton(R.string.button_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put(Master.ORG_ABBR, Organisations.organisationList.get(OrganisationAdapter.getLastCheckedPos()).getOrgabbr());
                            jsonObject.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());

                            new LeaveOrganisationTask(OrganisationAdapter.getLastCheckedPos()).execute(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();

                break;

            case R.id.bChangeOrganisation:

                for (int i = 0; i < Organisations.organisationList.size(); ++i) {
                    if (Organisations.organisationList.get(i).getIsChecked()) {

                        if(Organisations.organisationList.get(i).getName().equals(MemberDetails.getSelectedOrgName()))
                        {
                            Toast.makeText(getActivity(), R.string.toast_no_changes_to_save, Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(),
                                    Organisations.organisationList.get(i).getName(),
                                    Organisations.organisationList.get(i).getOrgabbr());

                            dbHelper.getProfile();


                            MemberDetails.setSelectedOrgAbbr(Organisations.organisationList.get(i).getOrgabbr());
                            MemberDetails.setSelectedOrgName(Organisations.organisationList.get(i).getName());
                            Master.CART_ITEM_COUNT = dbHelper.getCartItemsCount(MemberDetails.getMobileNumber(),
                                    MemberDetails.getSelectedOrgAbbr());
                            getActivity().invalidateOptionsMenu();
                            Toast.makeText(getActivity(), R.string.toast_organisation_changed_successfully, Toast.LENGTH_LONG).show();
                        }

                        break;
                    }
                }
                break;
        }
    }

    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType)
        {
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;

            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    class LeaveOrganisationTask extends AsyncTask<JSONObject, String, String>
    {
        int position;
        LeaveOrganisationTask(int position)
        {
            this.position = position;
        }
        @Override
        protected void onPreExecute() {
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_terminating_your_membership), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            Master.getJSON = new GetJSON();
//            System.out.println("URL: " + Master.getLeaveOrganisationURL());
//            System.out.println("PARAMS" + params[0]);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getLeaveOrganisationURL(), params[0], "POST", true,
                    MemberDetails.getEmail(), MemberDetails.getPassword());
//            System.out.println(Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String s) {

            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(OrganisationFragment.this.isAdded())
            {
                try {
                    Master.responseObject = new JSONObject(s);
                    if(Master.response.equals("exception"))
                    {
                        Toast.makeText(getActivity(), R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                    }
                    else if(Master.responseObject.getString(Master.RESPONSE).equals("Membership removed")
                            || Master.responseObject.getString(Master.RESPONSE).equals("error"))
                    {
                        Toast.makeText(getActivity(), R.string.toast_Membership_terminated_successfully, Toast.LENGTH_LONG).show();

                        dbHelper.deleteCart(MemberDetails.getMobileNumber(), Organisations.organisationList.get(position).getOrgabbr());

                        String org[] = new String[2];
                        org = dbHelper.getSelectedOrg(MemberDetails.getMobileNumber());

                        if(org[1].equals(Organisations.organisationList.get(position).getName()))
                        {
                            Organisations.organisationList.remove(position);
                            mAdapter.notifyItemRemoved(position);
                            Organisations.organisationList.get(0).setIsChecked(true);
                            mAdapter.notifyItemChanged(0);

                            dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(),
                                    Organisations.organisationList.get(0).getName(),
                                    Organisations.organisationList.get(0).getOrgabbr());
                        }
                        else
                        {
                            Organisations.organisationList.remove(position);
                            mAdapter.notifyItemRemoved(position);
                            Organisations.organisationList.get(0).setIsChecked(true);
                            mAdapter.notifyItemChanged(0);
                        }

                        setOrg();

                        if(Organisations.organisationList.size() == 1)
                        {
                            bLeaveOrganisation.setVisibility(View.GONE);
                            bChangeOrganisation.setVisibility(View.GONE);
                        }
                        else
                        {
                            bLeaveOrganisation.setVisibility(View.VISIBLE);
                            bChangeOrganisation.setVisibility(View.VISIBLE);
                        }

                    }
                    else if(Master.responseObject.getString(Master.RESPONSE).equals("Cannot be deleted"))
                    {
                        Toast.makeText(getActivity(), R.string.toast_Membership_cannot_be_terminated, Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //to set first org as selected if no org is selected org
    void setOrg()
    {
        boolean flag = false;
        for(int i = 0; i < Organisations.organisationList.size(); ++i)
        {
            if(Organisations.organisationList.get(i).getIsChecked())
            {
                flag = true;
                break;
            }
        }

        if(!flag)
        {
            dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(),
                    Organisations.organisationList.get(0).getName(),
                    Organisations.organisationList.get(0).getOrgabbr());

            Master.isMember = true;

            Organisations.organisationList.get(0).setIsChecked(true);
        }
    }

    public class GetOrganisationsTask extends AsyncTask<JSONObject, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_fetching_data_from_server), true);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJson = new GetJSON();
//            System.out.println("Organisations JSON: " + params[0]);
            Master.response = getJson.getJSONFromUrl(Master.getLoginURL(), params[0], "POST", false, null, null);
//            System.out.println("Organisations response: " + Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response) {



//            System.out.println("------------" + response);

            if(OrganisationFragment.this.isAdded())
            {
                if (response.equals("exception")) {
                    tNoData.setVisibility(View.VISIBLE);
                    ivNoData.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                    tOrganisationZero.setVisibility(View.GONE);
                    bChangeOrganisation.setVisibility(View.GONE);
                    bLeaveOrganisation.setVisibility(View.GONE);
                }
                else
                {
                    SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, response);
                    Organisations.organisationList = new ArrayList<Organisations>();
                    Organisations.organisationList = JSONDataHelper.getOrganisationListFromJson(getActivity(), response);

//                    System.out.println(Organisations.organisationList);

                    if(Organisations.organisationList.size() == 0)
                    {
                        mRecyclerView.setVisibility(View.GONE);
                        tOrganisationZero.setVisibility(View.VISIBLE);
                        bChangeOrganisation.setVisibility(View.GONE);
                        bLeaveOrganisation.setVisibility(View.GONE);
                        dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(), "null", "null");
                    }
                    else
                    {
                        if(Organisations.organisationList.size() == 1)
                        {
                            bLeaveOrganisation.setVisibility(View.GONE);
                            bChangeOrganisation.setVisibility(View.GONE);
                        }
                        else
                        {
                            bLeaveOrganisation.setVisibility(View.VISIBLE);
                            bChangeOrganisation.setVisibility(View.VISIBLE);
                        }
                        String org[] = new String[2];
                        org = dbHelper.getSelectedOrg(MemberDetails.getMobileNumber());

                        for(int i = 0; i < Organisations.organisationList.size(); ++i)
                        {
                            if(Organisations.organisationList.get(i).getName()
                                    .equals(org[1]))
                            {
                                Organisations.organisationList.get(i).setIsChecked(true);
                                break;
                            }
                        }

                        setOrg();

                        mAdapter = new OrganisationAdapter(getActivity());
                        mRecyclerView.setAdapter(mAdapter);
                    }
                }
            }

            if(Material.circularProgressDialog != null)
                Material.circularProgressDialog.dismiss();
        }
    }
}
